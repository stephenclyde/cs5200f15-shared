﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public abstract class NameValue : Object
    {
        private object myLock = new object();
        private string myKey;
        private object myValue;

        public string KeyName
        {
            get
            {
                string result;
                lock (myLock) { result = myKey; }
                return result;
            }
            set
            {
                lock (myLock) { myKey = value; }
            }
        }
        public Object Value
        {
            get
            {
                object result;
                lock (myLock) { result = myValue; }
                return result;
            }
            set
            {
                lock (myLock) { myValue = value; }
            }
        }

        public NameValue(string name) { KeyName = name; }
        public NameValue(string keyName, Object value)
        {
            KeyName = keyName;
            Value = value;
        }

        public virtual string DisplayValue()
        {
            string result = string.Empty;
            if (Value != null)
                result = Value.ToString();
            return result;
        }
    }
}
