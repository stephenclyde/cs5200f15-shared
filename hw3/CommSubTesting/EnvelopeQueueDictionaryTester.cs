﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommSub;
using Messages;
using SharedObjects;

namespace CommSubTesting
{
    [TestClass]
    public class EnvelopeQueueDictionaryTester
    {
        [TestMethod]
        public void EnvelopeQueueDictionary_TestConstructor()
        {
            EnvelopeQueueDictionary eqd = new EnvelopeQueueDictionary();
            Assert.IsNotNull(eqd);
            Assert.IsNotNull(eqd.RequestQueue);
            Assert.AreEqual(0, eqd.ConversationQueueCount);

            MessageNumber n1_2 = new MessageNumber() { ProcessId = 1, SeqNumber = 2 };
            EnvelopeQueue q1_2 = eqd.GetByName(n1_2);
            Assert.IsNull(q1_2);
        }

        [TestMethod]
        public void EnvelopeQueueDictionary_TestQueueManagement()
        {
            EnvelopeQueueDictionary eqd = new EnvelopeQueueDictionary();

            MessageNumber n00 = new MessageNumber();
            EnvelopeQueue queue01 = eqd.GetByName(n00);
            Assert.IsNull(queue01);

            queue01 = eqd.RequestQueue;
            Assert.IsNotNull(queue01);
            Assert.AreEqual(0, queue01.Count);

            MessageNumber n02 = new MessageNumber() { ProcessId = 1, SeqNumber = 3};
            EnvelopeQueue queue02 = eqd.CreateQueue(n02);
            Assert.IsNotNull(queue02);
            Assert.AreNotSame(queue01, queue02);
            Assert.AreEqual(0, queue02.Count);
            Assert.AreEqual(1, eqd.ConversationQueueCount);

            MessageNumber n03 = new MessageNumber() { ProcessId = 5, SeqNumber = 30 };
            EnvelopeQueue queue03 = eqd.CreateQueue(n03);
            Assert.IsNotNull(queue03);
            Assert.AreNotSame(queue01, queue03);
            Assert.AreNotSame(queue02, queue03);
            Assert.AreEqual(0, queue03.Count);
            Assert.AreEqual(2, eqd.ConversationQueueCount);

            MessageNumber n04 = new MessageNumber() { ProcessId = 1, SeqNumber = 3 };
            EnvelopeQueue queue04 = eqd.CreateQueue(n04);
            Assert.IsNotNull(queue04);
            Assert.AreSame(queue02, queue04);
            Assert.AreEqual(0, queue04.Count);
            Assert.AreEqual(2, eqd.ConversationQueueCount);

            MessageNumber n05 = new MessageNumber() { ProcessId = 1, SeqNumber = 3 };
            EnvelopeQueue queue05 = eqd.GetByName(n05);
            Assert.IsNotNull(queue05);
            Assert.AreSame(queue02, queue05);

            MessageNumber n06 = new MessageNumber() { ProcessId = 10, SeqNumber = 20 };
            EnvelopeQueue queue06 = eqd.GetByName(n06);
            Assert.IsNull(queue06);

            eqd.CloseQueue(n02);
            EnvelopeQueue queue07 = eqd.GetByName(n02);
            Assert.IsNull(queue07);

            // Close the requeue queue
            eqd.CloseQueue(n00);
            Assert.AreEqual(1, eqd.ConversationQueueCount);

            queue01 = eqd.GetByName(n00);
            Assert.IsNull(queue01);
        }
    }
}
