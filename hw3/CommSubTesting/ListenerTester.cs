﻿using System;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;

namespace CommSubTesting
{
    /// <summary>
    /// Summary description for ListenerTester
    /// </summary>
    [TestClass]
    public class ListenerTester
    {
 
        [TestMethod]
        public void Listener_TestEverything()
        {
            // Setup a fake process id
            MessageNumber.LocalProcessId = 10;

            // Create a commSubsystem with a Listener
            CommSubsystem commSubsystem = TestUtilities.SetupTestCommSubsystem(new DummyConversationFactory());

            // Get the EP for the commSubsystem, so we can send messages to it
            PublicEndPoint targetEP = new PublicEndPoint()
                            {
                              Host = "127.0.0.1",
                              Port = commSubsystem.Communicator.Port
                            };

            // Create another Communicator to send stuff to the listener.
            Communicator remoteComm = new Communicator() { MinPort = 12000, MaxPort = 12099 };
            remoteComm.Start();

            // Check initial state
            Assert.AreEqual(0, commSubsystem.QueueDictionary.RequestQueue.Count);
            Assert.AreEqual(0, commSubsystem.QueueDictionary.ConversationQueueCount);
            Assert.AreEqual(0, DummyConversation.CreatedInstances.Count);

            // Case 1 - Send a request message that is acceptable and will create a
            //          new conversation

            commSubsystem.Doer.Suspend();   // Suspend the doer so we can see the message in the RequestQueue
            Thread.Sleep(3000);

            MessageNumber nr1 = MessageNumber.Create();
            AliveRequest request = new AliveRequest() { MessageNr = nr1, ConversationId = nr1 };
            Envelope env1 = new Envelope() { Message = request, EP = targetEP };
            remoteComm.Send(env1);

            Thread.Sleep(1000);
            Assert.AreEqual(1, commSubsystem.QueueDictionary.RequestQueue.Count);

            commSubsystem.Doer.Resume();
            Thread.Sleep(3000);

            Assert.AreEqual(1, DummyConversation.CreatedInstances.Count);
            Assert.IsTrue(DummyConversation.LastCreatedInstance.ExecuteWasCalled);
            Assert.AreEqual(0, commSubsystem.QueueDictionary.RequestQueue.Count);

            // Case 2 - Send a message as part of an existing conversation

            MessageNumber convId = new MessageNumber() { ProcessId = 15, SeqNumber = 20 };
            EnvelopeQueue convQueue = commSubsystem.QueueDictionary.CreateQueue(convId);
                                                    // Simulate an existing conversation that able
                                                    // to receive message.
            Assert.AreEqual(0, convQueue.Count);

            MessageNumber nr2 = MessageNumber.Create();
            Reply reply = new Reply() { MessageNr = nr2, ConversationId = convId };
            Envelope env2 = new Envelope() { Message = reply, EP = targetEP };
            remoteComm.Send(env2);

            Thread.Sleep(1000);
            Assert.AreEqual(1, convQueue.Count);

            // Case 3 - Send a message of a type that can't start a conversation and isn't
            //          part of an existing conversation
            commSubsystem.Doer.Suspend();   // Suspend the doer so we can see the message in the RequestQueue
            Thread.Sleep(3000);

            MessageNumber nr3 = MessageNumber.Create();
            LoginRequest request3 = new LoginRequest() { MessageNr = nr3, ConversationId = nr3 };
            Envelope env3 = new Envelope() { Message = request, EP = targetEP };
            remoteComm.Send(env2);

            Thread.Sleep(1000);
            Assert.AreEqual(0, commSubsystem.QueueDictionary.RequestQueue.Count);

            commSubsystem.Doer.Resume();
            Thread.Sleep(3000);

            Assert.AreEqual(1, DummyConversation.CreatedInstances.Count);
        }



    }
}
