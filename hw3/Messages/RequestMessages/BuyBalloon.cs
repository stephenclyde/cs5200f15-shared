﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class BuyBalloon : Message
    {
        [DataMember]
        public Penny Penny { get; set; }
    }
}
