﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class ThrowBalloon
    {
        [DataMember]
        public Balloon Balloon { get; set; }
    }
}
