﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.AuctionMessages
{
    public class UmbrellaDelivery
    {
        public Umbrella Umbrella { get; set; }
    }
}
