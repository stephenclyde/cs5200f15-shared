﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Threading;

using Messages;
using SharedObjects;
using log4net;

namespace CommSub
{
    public class Communicator
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(Communicator));
        private static readonly ILog logDeep = LogManager.GetLogger(typeof(Communicator).ToString() + "_Deep");
        private const int networkReflectionTimeout = 2000;

        private UdpClient myUdpClient;

        private object myLock = new object();
        #endregion

        #region Public Properties
        public int MinPort { get; set; }
        public int MaxPort { get; set; }
        public int Port { get { return (myUdpClient != null) ? ((IPEndPoint)myUdpClient.Client.LocalEndPoint).Port : 0; } }

        #endregion

        #region Public Methods
        public void Start()
        {
            log.Debug("Start communicator");
            bool bindSuccessfull = false;

            ValidPorts();

            int portToTry = MinPort;
            while (!bindSuccessfull && portToTry <= MaxPort)
            {
                try
                {
                    IPEndPoint localEP = new IPEndPoint(IPAddress.Any, portToTry);
                    myUdpClient = new UdpClient(localEP);
                    bindSuccessfull = true;
                }
                catch (SocketException)
                {
                    portToTry++;
                }
            }
            if (!bindSuccessfull)
                throw new ApplicationException("Cannot bind the socket to a port");
        }

        public void Dispose()
        {
            log.Debug("Dispose of communicator");
            if (myUdpClient != null)
            {
                myUdpClient.Close();
                myUdpClient = null;
            }
        }

        public int IncomingAvailable()
        {
            return ((myUdpClient==null) ? 0 : myUdpClient.Available);
        }

        public Envelope GetIncoming(int timeout = 0)
        {
            Envelope result = null;

            IPEndPoint ep;
            byte[] receivedBytes = ReceiveBytes(timeout, out ep);
            if (receivedBytes != null && receivedBytes.Length>0)
            {
                PublicEndPoint pep = new PublicEndPoint() { IPEndPoint = ep };
                Message message = Message.Decode(receivedBytes);
                if (message != null && pep != null)
                {
                    result = new Envelope(message, pep);
                    log.DebugFormat("Just received message, Nr={0}, Conv={1}, Type={2}, From={3}",
                        (result.Message.MessageNr==null) ? "null" : result.Message.MessageNr.ToString(),
                        (result.Message.ConversationId==null) ? "null" : result.Message.ConversationId.ToString(),
                        result.Message.GetType().Name,
                        (result.IPEndPoint==null) ? "null" : result.IPEndPoint.ToString());
                }
                else
                {
                    log.ErrorFormat("Cannot decode message received from {0}", pep.ToString());
                    string tmp = Encoding.ASCII.GetString(receivedBytes);
                    log.ErrorFormat("Message={0}", tmp);
                }
            }
        
            return result;
        }

        public bool Send(Envelope outgoingEnvelope)
        {
            bool result = false;
            if (outgoingEnvelope == null || !outgoingEnvelope.IsValidToSend)
                log.Warn("Invalid Envelope or Message");
            else
            {
                //log.DebugFormat("Send message, Nr={0}, Conv={1}, Type={2}, To={3}",
                //            outgoingEnvelope.Message.MessageNr.ToString(),
                //            outgoingEnvelope.Message.ConvId.ToString(),
                //            outgoingEnvelope.Message.GetType(),
                //            outgoingEnvelope.IPEndPoint.ToString());

                byte[] bytesToSend =outgoingEnvelope.Message.Encode();

                log.DebugFormat("Send out: {0} to {1}", ASCIIEncoding.ASCII.GetString(bytesToSend), outgoingEnvelope.EP.ToString());

                try
                {
                    myUdpClient.Send(bytesToSend, bytesToSend.Length, outgoingEnvelope.EP.IPEndPoint);
                    result = true;
                    log.Debug("Send complete");
                }
                catch (Exception err)
                {
                    log.Warn(err.ToString());
                }
            }
            return result;
        }

        #endregion

        #region Private Methods

        private byte[] ReceiveBytes(int timeout, out IPEndPoint ep)
        {
            byte[] receivedBytes = null;
            ep = null;
            if (myUdpClient != null)
            {
                myUdpClient.Client.ReceiveTimeout = timeout;
                ep = new IPEndPoint(IPAddress.Any, 0);
                try
                {
                    logDeep.Debug("Try receive bytes from anywhere");
                    receivedBytes = myUdpClient.Receive(ref ep);
                    log.Debug("Back from receive");

                    if (log.IsDebugEnabled)
                    {
                        if (receivedBytes != null)
                        {
                            string tmp = ASCIIEncoding.ASCII.GetString(receivedBytes);
                            log.DebugFormat("Incoming message={0}", tmp);
                        }
                    }
                }
                catch (SocketException err)
                {
                    if (err.SocketErrorCode != SocketError.TimedOut && err.SocketErrorCode != SocketError.Interrupted)
                        log.Warn(err.Message);
                }
                catch (Exception err)
                {
                    log.Warn(err.Message);
                }
            }
            return receivedBytes;
        }

        private void ValidPorts()
        {
            if ((MinPort != 0 && (MinPort < IPEndPoint.MinPort || MinPort > IPEndPoint.MaxPort)) ||
                (MaxPort != 0 && (MaxPort < IPEndPoint.MinPort || MaxPort > IPEndPoint.MaxPort)))
                throw new ApplicationException("Invalid port specifications");
        }
        #endregion

    }
}
