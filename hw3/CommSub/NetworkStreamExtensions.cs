﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.IO;

using Messages.StreamMessages;
using SharedObjects;

using log4net;

namespace CommSub
{
    public static class NetworkStreamExtensions
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NetworkStreamExtensions));

        public static bool WriteStreamMessage(this NetworkStream stream, StreamMessage message)
        {
            bool result = false;
            log.DebugFormat("In WriteStreamMessage, message={0}", (message == null) ? "null" : message.GetType().Name);
            if (stream != null && message != null)
            {
                byte[] messageBytes = message.Encode();
                byte[] lengthBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(messageBytes.Length));
                if (stream.CanWrite)
                {
                    try
                    {
                        stream.Write(lengthBytes, 0, lengthBytes.Length);
                        stream.Write(messageBytes, 0, messageBytes.Length);
                        result = true;
                        log.Debug("Write complete");
                    }
                    catch (Exception err)
                    {
                        log.Error(err.Message);
                    }
                }
                else
                    log.Warn("Stream is not writable");
            }
            return result;
        }

        public static StreamMessage ReadStreamMessage(this NetworkStream stream)
        {
            log.DebugFormat("In ReadStreamMessage, with stream.ReadTimeout={0}", (stream==null) ? "null" : stream.ReadTimeout.ToString());

            StreamMessage result = null;

            int bytesRead = 4;
            byte[] bytes = new byte[4];
            bytes = ReadBytes(stream, bytesRead);

            log.DebugFormat("Length bytes read = {0}", bytes.Length);

            if (bytesRead == bytes.Length)
            {
                int messageLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
                log.DebugFormat("Incoming message will be {0} bytes", messageLength);

                bytes = ReadBytes(stream, messageLength);
                log.DebugFormat("Message bytes read = {0}", bytes.Length);

                if (messageLength == bytes.Length)
                    result = StreamMessage.Decode(bytes);
            }
            return result;
        }

        private static byte[] ReadBytes(NetworkStream stream, int bytesToRead)
        {
            byte[] bytes = new byte[bytesToRead];
            int bytesRead = 0;

            log.DebugFormat("Try to read {0} length bytes, with stream.CanRead={1} and stream.ReadTimeout={2}", bytesToRead, stream.CanRead, stream.ReadTimeout);

            int remainingTime = stream.ReadTimeout;
            while (stream.CanRead && bytesRead < bytesToRead && remainingTime > 0)
            {
                DateTime ts = DateTime.Now;
                try
                {
                    bytesRead += stream.Read(bytes, bytesRead, bytes.Length - bytesRead);
                }
                catch (IOException) { }
                catch (Exception err)
                {
                    log.Warn(err.GetType());
                    log.Warn(err.Message);
                }
                remainingTime -= Convert.ToInt32(DateTime.Now.Subtract(ts).TotalMilliseconds);
            }

            if (bytesToRead != bytesRead)
            {
                if (bytesRead > 0)
                    throw new ApplicationException(string.Format("Expected {0} bytes of messsage data, but only {1} of them arrived within {2} ms", bytesToRead, bytesRead, stream.ReadTimeout));
                bytes = new byte[0];
            }
            return bytes;
        }

    }
}
