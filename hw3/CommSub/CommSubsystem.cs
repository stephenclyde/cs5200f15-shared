﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommSub
{
    public class CommSubsystem
    {
        // A CommSubsystem is a facade that encapsulates the following objects.
        private EnvelopeQueueDictionary queueDictionary = null;
        private Communicator myCommunicator = null;
        private Listener myListener = null;
        private Doer myDoer = null;
        private ProcessAddressBook myKnownProcesses = null;

        // A process (e.g., something that specializes CommProcess) should set the following
        // properties when it creates a new subsystem.
        public int MinPort { get; set; }
        public int MaxPort { get; set; }
        public ConversationFactory ConversationFactory { get; set; }

        // The following properties are conveniences for working with the ComSubsystem.
        // They are part of the abstraction that this facade provides
        public EnvelopeQueueDictionary QueueDictionary { get { return queueDictionary; } }
        public Communicator Communicator { get { return myCommunicator; } }
        public Listener Listener { get { return myListener; } }
        public Doer Doer { get { return myDoer; } }
        public ProcessAddressBook KnownProcesses { get { return myKnownProcesses; } }

        /// <summary>
        /// Initialize
        /// 
        /// This methods setup up all of the components in a CommSubsystem.  Call this method
        /// sometime after setting the MinPort, MaxPort, and ConversationFactory
        /// </summary>
        public void Initialize()
        {
            queueDictionary = new EnvelopeQueueDictionary();
            
            ConversationFactory.Initialize();
            myCommunicator = new Communicator() { MinPort = MinPort, MaxPort = MaxPort };
            myListener = new Listener() { Label = "Listener" };
            myDoer = new Doer() { Label = "Doer" };

            myListener.CommSubsystem = this;
            myDoer.CommSubsystem = this;
            ConversationFactory.CommSubsystem = this;

            ClearKnownProcesses();
        }
        
        /// <summary>
        /// Start
        /// 
        /// This method starts up all active components in the CommSubsystem.  Call this method
        /// sometime after calling Initalize.
        /// </summary>
        public void Start()
        {
            myCommunicator.Start();
            myListener.Start();
            myDoer.Start();
        }

        /// <summary>
        /// Stop
        /// 
        /// This method stops all of the active components of a CommSubsystem and release the
        /// releases (or at least allows them to be garabage collected.  Once stop is called,
        /// a CommSubsystem cannot be restarted with setting it up from scratch.
        /// </summary>
        public void Stop()
        {
            if (myDoer != null)
            {
                myDoer.Stop();
                myDoer = null;
            }

            if (myListener != null)
            {
                myListener.Stop();
                myListener = null;
            }

            if (myCommunicator != null)
            {
                myCommunicator.Dispose();
                myCommunicator = null;
            }

            ClearKnownProcesses();
        }
        /// <summary>
        /// ClearKnownProcesses
        /// 
        /// This methods is clear the process address book.  It is used internally, but public used
        /// for testing purposes
        /// </summary>
        public void ClearKnownProcesses()
        {
            myKnownProcesses = new ProcessAddressBook();
        }

    }
}
