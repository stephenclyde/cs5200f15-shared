﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Messages;
using SharedObjects;
using CommSub;
using Utils;

using log4net;

namespace CommSub
{
    public class Doer : BackgroundThread
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(Doer));
        private int timeout_ms = 1000;
        #endregion

        public Doer() { }

        public CommSubsystem CommSubsystem { get; set; }

        protected override void Process(Object state)
        {
            while (keepGoing)
            {
                if (Suspended)
                    Thread.Sleep(100);
                else
                {
                    log.DebugFormat("Top of processing loop, RequestQueue.Count={0}", CommSubsystem.QueueDictionary.RequestQueue.Count);
                    Envelope incomingRequestEnvelope = CommSubsystem.QueueDictionary.RequestQueue.Dequeue(timeout_ms);
                    if (incomingRequestEnvelope != null && incomingRequestEnvelope.Message != null)
                        Dispatch(incomingRequestEnvelope);
                }
            }
        }

        private void Dispatch(Envelope incomingRequestEnvelope)
        {
            Conversation conversation = CommSubsystem.ConversationFactory.CreateFromMessageType(incomingRequestEnvelope.Message.GetType(), incomingRequestEnvelope);
            if (conversation == null)
                log.WarnFormat("Cannot find strategy for {0}", incomingRequestEnvelope.Message.GetType().Name);
            else
            {
                log.DebugFormat("Dispatch request to strategy, type={0}, message from={1}", conversation.GetType().Name, incomingRequestEnvelope.IPEndPoint);
                conversation.Launch();
            }
        }

    }
}
