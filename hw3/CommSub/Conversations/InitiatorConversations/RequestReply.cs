﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;

using log4net;
using Utils;

namespace CommSub.Conversations.InitiatorConversations
{
    public abstract class RequestReply : Conversation
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RequestReply));

        /// <summary>
        /// Execute a JoinGame Conversation.
        /// 
        /// Note that this is a template method, with three overridable parts:
        ///     IsConversationStateValid()
        ///     IsProcessStateValid()
        ///     CreateRequest()
        ///     
        /// Add it uses ReliableSend, which is another template method, with two override parts:
        ///     ReplyHandler
        ///     FailureHandler
        /// </summary>
        /// <param name="strategyContext">A GameInfo object for the game to join</param>
        public override void Execute(object context)
        {
            log.DebugFormat("In Execute for {0}", this.GetType().Name);
            Done = false;

            if (IsConversationStateValid())
            {
                if (IsProcessStateValid())
                {
                    log.DebugFormat("Start {0} conversation", this.GetType().Name);
                    Envelope env = CreateRequest();
                    if (env != null)
                    {
                        env.Message.InitMessageAndConversationNumbers();
                        ReliableSend(env);
                    }
                }
                else
                {
                    error = Error.Get(Error.StandardErrorNumbers.InvalidProcessStateForConversation);
                    ProcessState.AddError(error);
                    log.Warn(error.Message);
                }
            }
            else
            {
                error = Error.Get(Error.StandardErrorNumbers.InvalidConversationSetup);
                ProcessState.AddError(error);
                log.Warn(error.Message);
            }

            Done = true;
            log.DebugFormat("End {0} Conversation", this.GetType().Name);
        }

        protected virtual bool IsConversationStateValid() { return true; }

        protected virtual bool IsProcessStateValid() { return true; }

        protected abstract Envelope CreateRequest();
        
        protected void ReliableSend(Envelope envelope)
        {
            queueId = envelope.Message.MessageNr;
            EnvelopeQueue queue  = CommSubsystem.QueueDictionary.CreateQueue(queueId);

            log.DebugFormat("In reliable send, with Queue={0}", queueId);

            int retries = MaxRetries;

            while (retries > 0 && !ProcessState.Quit)
            {
                log.DebugFormat("Send envelope with a {0} to {1}", envelope.Message.GetType().Name, envelope.IPEndPoint.ToString());
                if (MyCommunicator.Send(envelope))
                {
                    log.DebugFormat("Try to get a response");
                    Envelope replyEnvelope = null;
                    if ((replyEnvelope = queue.Dequeue(Timeout)) != null)
                    {
                        log.DebugFormat("Got a reply {0} from {1}", replyEnvelope.Message.GetType().Name, replyEnvelope.IPEndPoint.ToString());
                        if (ReplyHandler(replyEnvelope))
                            break;
                        else
                            retries--;
                    }
                    else
                    {
                        log.DebugFormat("Nothing available in queue {0}", queue.QueueId);
                        retries--;
                    }
                }
                else
                {
                    log.Warn("Cannot send request");
                    retries = 0;
                }
            }
            if (retries == 0)
                FailureHandler();

            CommSubsystem.QueueDictionary.CloseQueue(queueId);
        }

        protected bool ReplyHandler(Envelope replyEnvelope)
        {
            log.Debug("Enter ReplyHandler");
            bool result = false;
            if (IsEnvelopeValid(replyEnvelope, true, ReplyProcessMustBeInGame, AllowedReplyTypes))
            {
                log.DebugFormat("Received a {0} message back", replyEnvelope.Message.GetType().Name);
                Reply reply = replyEnvelope.Message as Reply;
                if (reply.Success)
                {
                    ProcessReply(reply);
                }
                else
                {
                    string errorMessage = string.Format("{0} failed because {1}", this.GetType().Name, reply.Note);
                    ProcessState.AddError(new Error() { Message = errorMessage });
                    log.Warn(errorMessage);
                    PostFailureAction();
                }
                result = true;
            }
            else
            {
                string errorMessage = string.Format("{0} is an invalid reply for a {1}", replyEnvelope.Message.GetType().Name, this.GetType().Name);
                ProcessState.AddError(new Error() { Message = errorMessage });
                log.Warn(errorMessage);
            }

            return result;

        }

        protected virtual bool ReplyProcessMustBeInGame { get { return true; } }

        protected abstract Type[] AllowedReplyTypes { get; }

        protected virtual void ProcessReply(Reply reply) { }

        protected virtual void PostFailureAction() { }
        
        protected virtual void FailureHandler()
        {
            log.Debug("Enter Process Failure");
            
            string errorMessage = string.Format("{0} failed because no reply was received", this.GetType().Name);
            ProcessState.AddError(new Error() { Message = errorMessage });
            log.Warn(errorMessage);

            PostFailureAction();
        }

    }
}
