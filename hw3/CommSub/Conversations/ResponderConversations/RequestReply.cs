﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;
using Utils;

using log4net;

namespace CommSub.Conversations.ResponderConversations
{
    public abstract class RequestReply : Conversation
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RequestReply));

        /// <summary>
        /// Execute the responder side of a request-reply conversation
        /// 
        /// Note that this is a temple method, four override parts:
        ///     - AllowTypes
        ///     - RemoteProcessMustBeKnown
        ///     - RemoteProcessMustBeInGame
        ///     - IsConverstationStateValid
        ///     - IsProcessStateValid
        ///     - CreateReply
        /// </summary>
        /// <param name="context"></param>
        public override void Execute(object context)
        {
            Done = false;
            log.DebugFormat("Start {0} Conversation", this.GetType().Name);

            if (IsEnvelopeValid(IncomingEnvelope, RemoteProcessMustBeInGame, RemoteProcessMustBeKnown, AllowedTypes))
            {
                if (!IsConversationStateValid())
                    error = Error.Get(Error.StandardErrorNumbers.InvalidConversationSetup);
                else if (!IsProcessStateValid())
                    error = Error.Get(Error.StandardErrorNumbers.InvalidProcessStateForConversation);
                else
                {
                    log.DebugFormat("Reply to {0} message from {1}", IncomingEnvelope.Message.GetType().Name, IncomingEnvelope.EP.ToString());

                    Envelope reply = CreateReply();
                    reply.Message.SetMessageAndConversationNumbers(MessageNumber.Create(), IncomingEnvelope.Message.ConversationId);
                    UnreliableSend(reply);

                    log.DebugFormat("Replied with a {0}", reply.GetType().Name);
                }
            }

            if (error != null)
            {
                log.Warn(error.Message);
                ProcessState.AddError(error);
            }

            Done = true;
            log.DebugFormat("End {0}", this.GetType().Name);
        }

        protected virtual bool RemoteProcessMustBeKnown { get { return true; } }

        protected virtual bool RemoteProcessMustBeInGame { get { return true; } }

        protected abstract Type[] AllowedTypes { get; }

        protected virtual bool IsConversationStateValid() { return true; }

        protected virtual bool IsProcessStateValid() { return true; }

        protected abstract Envelope CreateReply();
    }
}
